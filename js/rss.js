function manipulateNews( response ) {

    var newsNum   = response.length;
    var parent    = document.getElementById('news');
    var parentMob = document.getElementById('newsMob');

    for ( var i = 0 ; i < newsNum ; i++ ) {

        var newsBoxElem = document.createElement("DIV");
        newsBoxElem.setAttribute( 'class', 'newsBox' );
        parent.appendChild( newsBoxElem );

        var mediaElem = document.createElement("A");
        mediaElem.setAttribute( 'href', response[ i ][ 'link' ] );
        mediaElem.setAttribute( 'target', '_blank' );
        mediaElem.setAttribute( 'class', 'media' );
        mediaElem.setAttribute( 'rel', 'noopener' );
        mediaElem.textContent = response[ i ][ 'source' ];
        newsBoxElem.appendChild( mediaElem );

        var titleElem = document.createElement("A");
        titleElem.setAttribute( 'href', response[ i ][ 'link' ] );
        titleElem.setAttribute( 'target', '_blank' );
        titleElem.setAttribute( 'class', 'title' );
        titleElem.setAttribute( 'rel', 'noopener' );
        titleElem.textContent = response[ i ][ 'title' ];
        newsBoxElem.appendChild( titleElem );

    }

};

manipulateNews( rss );
