function clickCountry( event ) {

    window.scrollTo({top: 0, behavior: 'smooth'});

    createCitiesData( event.currentTarget.getAttribute( 'data-countryname' ) );

}

function createTable( data ) {
    // console.log( data );
    var parent = document.getElementById('countriesTable');

    var countriesNum = data.length;

    for ( var i = 0 ; i < countriesNum ; i++ ) {

        var country       = data[ i ][ 0 ];
        var countryData   = data[ i ][ 1 ];
        var countryLocalName = translateCountry( country );

        var countryElem = document.createElement("DIV");
        countryElem.setAttribute( 'class', 'country' );
        countryElem.setAttribute( 'data-countryname', country );
        parent.appendChild( countryElem );

        countryElem.addEventListener( 'click', clickCountry );

        var nameElem = document.createElement("DIV");
        nameElem.setAttribute( 'class', 'name' );
        nameElem.textContent = countryLocalName;
        countryElem.appendChild( nameElem );

        var casesElem = document.createElement("DIV");
        casesElem.setAttribute( 'class', 'cases' );
        casesElem.setAttribute( 'title', countryLocalName + ': ' + countryData[ 'confirmed' ] + ' ' + translateText( 'cases reported until now' ) );
        countryElem.appendChild( casesElem );

        var casesElemText = document.createElement("P");
        casesElemText.textContent = countryData[ 'confirmed' ];
        casesElem.appendChild( casesElemText );

        var casesElemNewText = document.createElement("P");
        casesElem.appendChild( casesElemNewText );

        var recoveredElem = document.createElement("DIV");
        recoveredElem.setAttribute( 'class', 'recovered' );
        recoveredElem.setAttribute( 'title', countryLocalName + ': ' + countryData[ 'recovered' ] + ' ' + translateText( 'patients have recovered until now' ) );
        countryElem.appendChild( recoveredElem );

        var recoveredElemText = document.createElement("P");
        recoveredElemText.textContent = countryData[ 'recovered' ];
        recoveredElem.appendChild( recoveredElemText );

        var recoveredElemNewText = document.createElement("P");
        recoveredElem.appendChild( recoveredElemNewText );

        var deathsElem = document.createElement("DIV");
        deathsElem.setAttribute( 'class', 'deaths' );
        deathsElem.setAttribute( 'title', countryLocalName + ': ' + countryData[ 'deaths' ] + ' ' + translateText( 'deaths have been reported' ) );
        countryElem.appendChild( deathsElem );

        var deathsElemText = document.createElement("P");
        deathsElemText.textContent = countryData[ 'deaths' ];
        deathsElem.appendChild( deathsElemText );

        var deathsElemNewText = document.createElement("P");
        deathsElem.appendChild( deathsElemNewText );

        var newcasesElem = document.createElement("DIV");
        newcasesElem.setAttribute( 'class', 'newcases' );
        newcasesElem.setAttribute( 'title', countryLocalName + ': ' + countryData[ 'newcases' ] + ' ' + translateText( 'new cases have been reported during the last 24 hours' ) );
        countryElem.appendChild( newcasesElem );

        var newrecoveredElem = document.createElement("DIV");
        newrecoveredElem.setAttribute( 'class', 'newrecovered' );
        newrecoveredElem.setAttribute( 'title', countryLocalName + ': ' + countryData[ 'newrecoveries' ] + ' ' + translateText( 'people have recovered during the last 24 hours' ) );
        countryElem.appendChild( newrecoveredElem );

        var newdeathsElem = document.createElement("DIV");
        newdeathsElem.setAttribute( 'class', 'newdeaths' );
        newdeathsElem.setAttribute( 'title', countryLocalName + ': ' + countryData[ 'newdeaths' ] + ' ' + translateText( 'people have died during the last 24 hours' ) );
        countryElem.appendChild( newdeathsElem );

        if ( countryData[ 'confirmed' ] === countryData[ 'recovered' ] + countryData[ 'deaths' ] ) {

            countryElem.classList.add('green');

        }

        if ( countryData[ 'newcases' ] > 0 ) {

            newcasesElem.textContent = '+' + countryData[ 'newcases' ];
            casesElemNewText.textContent = '+' + countryData[ 'newcases' ];

        } else {

            newcasesElem.textContent = '0';
            newcasesElem.classList.add('green');
            casesElemNewText.textContent = '0';
            casesElemNewText.classList.add('green');
            newcasesElem.setAttribute( 'title', countryLocalName + translateText( ': 0 new cases have been reported during the last 24 hours' ) );

        }

        if ( countryData[ 'newrecoveries' ] > 0 ) {

            newrecoveredElem.textContent = '+' + countryData[ 'newrecoveries' ];
            recoveredElemNewText.textContent = '+' + countryData[ 'newrecoveries' ];

        } else {

            newrecoveredElem.textContent = '0';
            recoveredElemNewText.textContent = '0';

        }

        if ( countryData[ 'newdeaths' ] > 0 ) {

            newdeathsElem.textContent = '+' + countryData[ 'newdeaths' ];
            newdeathsElem.classList.add('red');

            deathsElemNewText.textContent = '+' + countryData[ 'newdeaths' ];
            deathsElemNewText.classList.add('red');

        } else {

            newdeathsElem.textContent = '0';
            deathsElemNewText.textContent = '0';

        }

    }

}

function sort_confirmed_descending( a, b ) {

    return b[1].confirmed - a[1].confirmed;

}

function sort_confirmed_ascending( a, b ) {

    return a[1].confirmed - b[1].confirmed;

}

function sort_recovered_descending( a, b ) {

    return b[1].recovered - a[1].recovered;

}

function sort_recovered_ascending( a, b ) {

    return a[1].recovered - b[1].recovered;

}

function sort_deaths_descending( a, b ) {

    return b[1].deaths - a[1].deaths;

}

function sort_deaths_ascending( a, b ) {

    return a[1].deaths - b[1].deaths;

}

function sort_newcases_descending( a, b ) {

    return b[1].newcases - a[1].newcases;

}

function sort_newcases_ascending( a, b ) {

    return a[1].newcases - b[1].newcases;

}

function sort_newrecoveries_descending( a, b ) {

    return b[1].newrecoveries - a[1].newrecoveries;

}

function sort_newrecoveries_ascending( a, b ) {

    return a[1].newrecoveries - b[1].newrecoveries;

}

function sort_newdeaths_descending( a, b ) {

    return b[1].newdeaths - a[1].newdeaths;

}

function sort_newdeaths_ascending( a, b ) {

    return a[1].newdeaths - b[1].newdeaths;

}

function sort_region_descending( a, b ) {

    return b[0].localeCompare( a[0] );

}

function sort_region_ascending( a, b ) {

    return a[0].localeCompare( b[0] );

}

function determineSortOrder( data ) {

    var headingElem = document.querySelector( '#countries .heading .sort' );

    var column = headingElem.getAttribute( 'data-column' );

    var order = headingElem.getAttribute( 'data-sort' );

    // var dataArr = Object.keys(data).map(key => {
    //     return data[key];
    // });

    var dataArr = Object.keys(data).map(function (key) {
        return [String(key), data[key]];
    });

    // console.log( dataArr[0] );

    var sortedData = null;

    if ( column === 'confirmed' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_confirmed_descending  );

    } else if ( column === 'confirmed' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_confirmed_ascending  );

    } else if ( column === 'recovered' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_recovered_descending  );

    } else if ( column === 'recovered' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_recovered_ascending  );

    } else if ( column === 'deaths' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_deaths_descending  );

    } else if ( column === 'deaths' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_deaths_ascending  );

    } else if ( column === 'newcases' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_newcases_descending  );

    } else if ( column === 'newcases' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_newcases_ascending  );

    } else if ( column === 'newrecoveries' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_newrecoveries_descending  );

    } else if ( column === 'newrecoveries' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_newrecoveries_ascending  );

    } else if ( column === 'newdeaths' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_newdeaths_descending  );

    } else if ( column === 'newdeaths' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_newdeaths_ascending  );

    } else if ( column === 'region' && order === 'descending' ) {

        sortedData = dataArr.sort( sort_region_descending  );

    } else if ( column === 'region' && order === 'ascending' ) {

        sortedData = dataArr.sort( sort_region_ascending  );

    } else {

        sortedData = dataArr;

    }

    // console.log( sortedData );

    createTable( sortedData );

}

function tableButtonsClick( event ) {

    var parent = document.getElementById('countriesTable');

    while ( parent.hasChildNodes() ) {

        parent.removeChild( parent.lastChild );

    }

    var currentOrder = event.currentTarget.getAttribute( 'data-sort' );

    var newOrder = null;

    if ( currentOrder === 'none' ) {

        newOrder = 'descending';

    } else if ( currentOrder === 'ascending' ) {

        newOrder = 'descending';

    } else if ( currentOrder === 'descending' ) {

        newOrder = 'ascending';

    }

    var buttons = document.querySelectorAll( '#countries .heading > div' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove( 'sort', 'ascending', 'descending' );
        buttons[ i ].setAttribute( 'data-sort', 'none' );

    }

    event.currentTarget.classList.add( 'sort', newOrder );

    event.currentTarget.setAttribute( 'data-sort', newOrder );

    determineSortOrder( data );

}

function bindTableHeadings() {

  var buttons = document.querySelectorAll( '#countries .heading > div' );
  var buttonsNum = buttons.length;

  for ( var i = 0 ; i < buttonsNum ; i++ ) {

      buttons[ i ].addEventListener( 'click', tableButtonsClick );

  }

}




determineSortOrder( data );

bindTableHeadings();
