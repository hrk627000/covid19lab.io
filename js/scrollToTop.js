window.onscroll = function() {scroll()};

document.getElementById('scrollToTop').addEventListener( 'click', scrollToTop );

function scrollToTop() {

    window.scrollTo({top: 0, behavior: 'smooth'});

}

function scroll() {
    if (
        document.body.scrollTop > 0 ||
        document.documentElement.scrollTop > 0
    ) {

        document.getElementById('scrollToTop').style.display = 'block';

    } else if (
        document.body.scrollTop == 0 ||
        document.documentElement.scrollTop == 0
    ) {

        document.getElementById('scrollToTop').style.display = 'none';

    }
}

scroll();
