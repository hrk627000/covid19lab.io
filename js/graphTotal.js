var graph;

var graphRaw = new Array();

google.charts.load('current', {
    'packages':['corechart']
});

function createGraph ( data, color ) {

    if ( typeof graph !== 'undefined' ) {

        graph.clearChart();

    }

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( data );

      var options = {
        chartArea: {width: '95%', height: '95%'},
        title: 'Cases',
        legend: { position: 'none' },
        hAxis: { textPosition: 'none', gridlines: { count: 0 }, baselineColor: 'none' },
        vAxis: { format: 'short', textPosition: 'in', gridlines: { minSpacing: 2, color: '#202020' }, minorGridlines: { count: 0, color: '#202020' }, baselineColor: 'none' },
        animation: { startup: true, duration: 1000, easing: 'out' },
        backgroundColor: '#181818',
        colors: [ color ],
        pointSize: 3
      };

      graph = new google.visualization.LineChart(document.getElementById('graph_chart'));

      graph.draw(dataGoogle, options);

    });

};

function populateChartData( response, field ) {

    var graphRaw = new Array();

    if ( field === 'confirmed' ) {

        graphRaw[0] = [ 'Date', 'Cases' ];

    } else if ( field === 'deaths' ) {

        graphRaw[0] = [ 'Date', 'Deaths' ];

    } else if ( field === 'newconfirmed' ) {

        graphRaw[0] = [ 'Date', 'New Cases' ];

    } else if ( field === 'newdeaths' ) {

        graphRaw[0] = [ 'Date', 'New Deaths' ];

    }

    for ( var block in response ) {

        var graphDataLoc = new Array();
        graphDataLoc.push( block ); //console.log( block );
        graphDataLoc.push( response[ block ][ field ] ); //console.log( response[ block ][ field ] );

        graphRaw.push( graphDataLoc );

    }

    return graphRaw;

}

function createGraphData( response, field, country = 'world' ) {

    var color = '#eee';

    if ( field === 'deaths' ) {

        color = '#FF3F3F';

    } else if ( field === 'newdeaths' ) {

        color = '#FF3F3F';

    }

    if ( country === 'world' ) {

        createGraph( populateChartData( response, field ), color );

    } else {

        var xhr = new XMLHttpRequest();
        xhr.open( 'GET', '/data/' + country + '.json', true );

        xhr.onload = function() {

            if ( this.status === 200 ) {

                var response = JSON.parse( this.response );

                createGraph( populateChartData( response, field ), color );

            }

        };

        xhr.send();

    }

};

function manipulateGraph() {

    var vw = Math.max( document.documentElement.clientWidth, window.innerWidth || 0) ;

    if ( vw > 0 && vw < 1023 ) {

        var graphPanel = document.getElementById( 'graphPanel' );

        var countriesPanel = document.getElementById( 'countries' );

        countriesPanel.parentNode.insertBefore( graphPanel, countriesPanel );

    }

    createGraphData( graphData, 'confirmed' );

};



function graphButtonsReset( event ) {

    var buttons = document.querySelectorAll( '#graphPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

    buttons[ 0 ].classList.add('active');

}

function graphButtonsClick( event ) {

    var buttons = document.querySelectorAll( '#graphPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

    event.target.classList.add('active');

    createGraphData( graphData, event.target.getAttribute( 'data-show' ), document.querySelector( '#mapPanel .mapCountryName' ).getAttribute( 'data-countrycode' ) );

};

function bindGraphButtons() {

    var buttons = document.querySelectorAll( '#graphPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].addEventListener( 'click', graphButtonsClick );

    }

};

manipulateGraph();

bindGraphButtons();
