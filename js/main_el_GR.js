var chart;

var dataRaw = new Array();

function btn_cases_update( num ) {

    document.getElementById('btn_cases').textContent = num.toLocaleString();
    document.getElementById('btn_cases').setAttribute('data-cases', num);

};

function btn_recovered_update( num ) {

    document.getElementById('btn_recovered').textContent = num.toLocaleString();
    document.getElementById('btn_recovered').setAttribute('data-recovered', num);

    document.getElementById('btn_closedRecovered').textContent = num.toLocaleString();
    document.getElementById('btn_closedRecovered').setAttribute('data-recovered', num);

};

function btn_deaths_update( num ) {

    document.getElementById('btn_deaths').textContent = num.toLocaleString();
    document.getElementById('btn_deaths').setAttribute('data-deaths', num);

    document.getElementById('btn_closedDeaths').textContent = num.toLocaleString();
    document.getElementById('btn_closedDeaths').setAttribute('data-closedDeaths', num);

};

function info_recoveredPercentage_update( num ) {

    document.getElementById('info_recoveredPercentage').textContent = num + '%';
    document.getElementById('info_recoveredPercentage').setAttribute('data-recoveredPercentage', num);

};

function info_deathsPercentage_update( num ) {

    document.getElementById('info_deathsPercentage').textContent = num + '%';
    document.getElementById('info_deathsPercentage').setAttribute('data-deathsPercentage', num);

};

function btn_closedCases_update( num ) {

    document.getElementById('btn_closedCases').textContent = num.toLocaleString();
    document.getElementById('btn_closedCases').setAttribute('data-closedCases', num);

};

function btn_countriesNum_update( num ) {

    document.getElementById('btn_countriesNum').textContent = num;
    document.getElementById('btn_countriesNum').setAttribute('data-countriesNum', num);

};

function btn_activeCases_update( num ) {

    document.getElementById('btn_activeCases').textContent = num.toLocaleString();
    document.getElementById('btn_activeCases').setAttribute('data-activeCases', num);

};

function mapButtonsClick( event ) {

    chart.clearChart();

    var buttons = document.querySelectorAll( '#mapPanel button' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

    event.target.classList.add('active');

    createMap( dataRaw, event.target.getAttribute('data-region') );

};

function bindButtons() {

    var buttons = document.querySelectorAll( '#mapPanel button' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].addEventListener( 'click', mapButtonsClick );

    }

};

function createMap ( data, region ) {

    var options = {
        colorAxis: {colors: ['yellow', 'orange', 'red']},
        backgroundColor: '#181818',
        datalessRegionColor: '#ddd'
    };

    google.charts.load('current', {
        'packages':['geochart']
    });

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( data );

      var options = {
          colorAxis: {colors: ['#FFACAC', '#FF3434']},
          backgroundColor: '#181818',
          datalessRegionColor: '#ddd',
          magnifyingGlass: {enable: true, zoomFactor: 5.0},
          region: region
      };

      chart = new google.visualization.GeoChart(document.getElementById('map'));

      chart.draw(dataGoogle, options);

    });

};

function createMapData( response ) {

    dataRaw = new Array();

    dataRaw[0] = [ 'Country', 'Cases', 'Deaths' ];

    for ( var country in response ) {

        var countryData = new Array();
        countryData.push( country );
        countryData.push( response[ country ][ 'confirmed' ] );
        countryData.push( response[ country ][ 'deaths' ] );

        dataRaw.push( countryData );

    }

    createMap( dataRaw, 'world' );

};

function manipulateCountries( response ) {

    createMapData( response );

    var cases         = 0;
    var recovered     = 0;
    var deaths        = 0;
    var closedCases   = 0;
    var recoveredPerc = 0;
    var deathsPerc    = 0;
    var countries     = 0;
    var activeCases   = 0;

    for ( var country in response ) {

        cases       = cases       + response[ country ][ 'confirmed' ];
        recovered   = recovered   + response[ country ][ 'recovered' ];
        deaths      = deaths   + response[ country ][ 'deaths' ];
        countries   = countries + 1;

        var parent = document.getElementById('countries');

        var countryElem = document.createElement("DIV");
        countryElem.setAttribute( 'class', 'country' );
        parent.appendChild( countryElem );

        if ( response[ country ][ 'confirmed' ] === response[ country ][ 'recovered' ] + response[ country ][ 'deaths' ] ) {

            countryElem.classList.add('green');

        }

        var nameElem = document.createElement("DIV");
        nameElem.setAttribute( 'class', 'name' );
        countryElem.appendChild( nameElem );

        var countryName;

        if ( country === 'China' ) { countryName = 'Κίνα' }
        else if ( country === 'South Korea' ) { countryName = 'Νότια Κορέα' }
        else if ( country === 'Italy' ) { countryName = 'Ιταλία' }
        else if ( country === 'Iran' ) { countryName = 'Ιράν' }
        else if ( country === 'France' ) { countryName = 'Γαλλία' }
        else if ( country === 'Germany' ) { countryName = 'Γερμανία' }
        else if ( country === 'Spain' ) { countryName = 'Ισπανία' }
        else if ( country === 'Others' ) { countryName = 'Princess Diamond' }
        else if ( country === 'United States' ) { countryName = 'ΗΠΑ' }
        else if ( country === 'Japan' ) { countryName = 'Ιαπωνία' }
        else if ( country === 'Switzerland' ) { countryName = 'Ελβετία' }
        else if ( country === 'United Kingdom' ) { countryName = 'Μεγάλη Βρετανία' }
        else if ( country === 'Netherlands' ) { countryName = 'Ολλανδία' }
        else if ( country === 'Belgium' ) { countryName = 'Βέλγιο' }
        else if ( country === 'Sweden' ) { countryName = 'Σουηδία' }
        else if ( country === 'Norway' ) { countryName = 'Νορβηγία' }
        else if ( country === 'Singapore' ) { countryName = 'Σιγκαπούρη' }
        else if ( country === 'Canada' ) { countryName = 'Καναδάς' }
        else if ( country === 'Malaysia' ) { countryName = 'Μαλαισία' }
        else if ( country === 'Hong Kong' ) { countryName = 'Χονγκ Κονγκ' }
        else if ( country === 'Austria' ) { countryName = 'Αυστρία' }
        else if ( country === 'Bahrain' ) { countryName = 'Μπαχρέιν' }
        else if ( country === 'Australia' ) { countryName = 'Αυστραλία' }
        else if ( country === 'Greece' ) { countryName = 'Ελλάδα' }
        else if ( country === 'Kuwait' ) { countryName = 'Κουβέιτ' }
        else if ( country === 'Iraq' ) { countryName = 'Ιράκ' }
        else if ( country === 'Iceland' ) { countryName = 'Ισλανδία' }
        else if ( country === 'Egypt' ) { countryName = 'Αίγυπτος' }
        else if ( country === 'Thailand' ) { countryName = 'Ταϋλάνδη' }
        else if ( country === 'United Arab Emirates' ) { countryName = 'Ην. Αραβικά Εμιράτα' }
        else if ( country === 'Taiwan' ) { countryName = 'Ταϊβάν' }
        else if ( country === 'India' ) { countryName = 'Ινδία' }
        else if ( country === 'Israel' ) { countryName = 'Ισραήλ' }
        else if ( country === 'San Marino' ) { countryName = 'Σαν Μαρίνο' }
        else if ( country === 'Denmark' ) { countryName = 'Δανία' }
        else if ( country === 'Lebanon' ) { countryName = 'Λίβανος' }
        else if ( country === 'Czech Republic' ) { countryName = 'Τσεχία' }
        else if ( country === 'Finland' ) { countryName = 'Φινλανδία' }
        else if ( country === 'Vietnam' ) { countryName = 'Βιετνάμ' }
        else if ( country === 'Portugal' ) { countryName = 'Πορτογαλία' }
        else if ( country === 'Brazil' ) { countryName = 'Βραζιλία' }
        else if ( country === 'Palestine' ) { countryName = 'Παλαιστίνη' }
        else if ( country === 'Ireland' ) { countryName = 'Ιρλανδία' }
        else if ( country === 'Algeria' ) { countryName = 'Αλγερία' }
        else if ( country === 'Philippines' ) { countryName = 'Φιλιππίνες' }
        else if ( country === 'Indonesia' ) { countryName = 'Ινδονησία' }
        else if ( country === 'Qatar' ) { countryName = 'Κατάρ' }
        else if ( country === 'Russia' ) { countryName = 'Ρωσία' }
        else if ( country === 'Slovenia' ) { countryName = 'Σλοβενία' }
        else if ( country === 'Oman' ) { countryName = 'Ομάν' }
        else if ( country === 'Poland' ) { countryName = 'Πολωνία' }
        else if ( country === 'Saudi Arabia' ) { countryName = 'Σαουδική Αραβία' }
        else if ( country === 'Romania' ) { countryName = 'Ρουμανία' }
        else if ( country === 'Ecuador' ) { countryName = 'Εκουαδόρ' }
        else if ( country === 'Georgia' ) { countryName = 'Γεωργία' }
        else if ( country === 'Argentina' ) { countryName = 'Αργεντινή' }
        else if ( country === 'Croatia' ) { countryName = 'Κροατία' }
        else if ( country === 'Macau' ) { countryName = 'Μακάο' }
        else if ( country === 'Estonia' ) { countryName = 'Εσθονία' }
        else if ( country === 'Azerbaijan' ) { countryName = 'Αζερμπαϊτζάν' }
        else if ( country === 'Costa Rica' ) { countryName = 'Κόστα Ρίκα' }
        else if ( country === 'Hungary' ) { countryName = 'Ουγγαρία' }
        else if ( country === 'Chile' ) { countryName = 'Χιλή' }
        else if ( country === 'Peru' ) { countryName = 'Περού' }
        else if ( country === 'Mexico' ) { countryName = 'Μεξικό' }
        else if ( country === 'Pakistan' ) { countryName = 'Πακιστάν' }
        else if ( country === 'Latvia' ) { countryName = 'Λετονία' }
        else if ( country === 'Belarus' ) { countryName = 'Λευκορωσία' }
        else if ( country === 'French Guiana' ) { countryName = 'Γαλλική Γουιάνα' }
        else if ( country === 'New Zealand' ) { countryName = 'Νέα Ζηλανδία' }
        else if ( country === 'Dominican Republic' ) { countryName = 'Δομινικανή Δημοκρατία' }
        else if ( country === 'Afghanistan' ) { countryName = 'Αφγανιστάν' }
        else if ( country === 'Bulgaria' ) { countryName = 'Βουλγαρία' }
        else if ( country === 'Maldives' ) { countryName = 'Μαλδίβες' }
        else if ( country === 'Senegal' ) { countryName = 'Σενεγάλη' }
        else if ( country === 'Bosnia and Herzegovina' ) { countryName = 'Βοσνία Ερζεγοβίνη' }
        else if ( country === 'North Macedonia' ) { countryName = 'Βόρεια Μακεδονία' }
        else if ( country === 'Bangladesh' ) { countryName = 'Μπαγκλαντές' }
        else if ( country === 'Slovakia' ) { countryName = 'Σλοβακία' }
        else if ( country === 'South Africa' ) { countryName = 'Νότια Αφρική' }
        else if ( country === 'Luxembourg' ) { countryName = 'Λουξεμβούργο' }
        else if ( country === 'Malta' ) { countryName = 'Μάλτα' }
        else if ( country === 'Cambodia' ) { countryName = 'Καμπότζη' }
        else if ( country === 'Cameroon' ) { countryName = 'Καμερούν' }
        else if ( country === 'Albania' ) { countryName = 'Αλβανία' }
        else if ( country === 'St. Martin' ) { countryName = 'Άγιος Μαρτίνος' }
        else if ( country === 'Faroe Islands' ) { countryName = 'Νήσοι Φερόες' }
        else if ( country === 'Nigeria' ) { countryName = 'Νιγηρία' }
        else if ( country === 'Morocco' ) { countryName = 'Μαρόκο' }
        else if ( country === 'Tunisia' ) { countryName = 'Τυνησία' }
        else if ( country === 'Martinique' ) { countryName = 'Μαρτινίκα' }
        else if ( country === 'Lithuania' ) { countryName = 'Λιθουανία' }
        else if ( country === 'Saint Barthelemy' ) { countryName = 'Άγιος Βαρθολομαίος' }
        else if ( country === 'Monaco' ) { countryName = 'Μονακό' }
        else if ( country === 'Togo' ) { countryName = 'Τόνγκο' }
        else if ( country === 'Armenia' ) { countryName = 'Αρμενία' }
        else if ( country === 'Ukraine' ) { countryName = 'Ουκρανία' }
        else if ( country === 'Vatican City' ) { countryName = 'Βατικανό' }
        else if ( country === 'Liechtenstein' ) { countryName = 'Λίχτενασταϊν' }
        else if ( country === 'Andorra' ) { countryName = 'Ανδόρρα' }
        else if ( country === 'Gibraltar' ) { countryName = 'Γιβραλτάρ' }
        else if ( country === 'Moldova' ) { countryName = 'Μολδαβία' }
        else if ( country === 'Colombia' ) { countryName = 'Κολομβία' }
        else if ( country === 'Bhutan' ) { countryName = 'Μπουτάν' }
        else if ( country === 'Nepal' ) { countryName = 'Νεπάλ' }
        else if ( country === 'Jordan' ) { countryName = 'Ιορδανία' }
        else if ( country === 'Paraguay' ) { countryName = 'Παραγουάη' }
        else if ( country === 'Serbia' ) { countryName = 'Σερβία' }
        else if ( country === 'Sri Lanka' ) { countryName = 'Σρι Λάνκα' }

        else { countryName = country; }

        nameElem.textContent = countryName;

        var casesElem = document.createElement("DIV");
        casesElem.setAttribute( 'class', 'cases' );
        casesElem.setAttribute( 'title', countryName + ': ' + response[ country ][ 'confirmed' ] + ' καταγεγραμμένα κρούσματα' );
        countryElem.appendChild( casesElem );

        var casesElemText = document.createElement("P");
        casesElemText.textContent = response[ country ][ 'confirmed' ];
        casesElem.appendChild( casesElemText );

        var casesElemNewText = document.createElement("P");
        casesElem.appendChild( casesElemNewText );

        var recoveredElem = document.createElement("DIV");
        recoveredElem.setAttribute( 'class', 'recovered' );
        recoveredElem.setAttribute( 'title', countryName + ': ' + response[ country ][ 'recovered' ] + ' περιπτώσεις ασθενών που ξεπέρασαν την ασθένεια' );
        countryElem.appendChild( recoveredElem );

        var recoveredElemText = document.createElement("P");
        recoveredElemText.textContent = response[ country ][ 'recovered' ];
        recoveredElem.appendChild( recoveredElemText );

        var recoveredElemNewText = document.createElement("P");
        recoveredElem.appendChild( recoveredElemNewText );

        var deathsElem = document.createElement("DIV");
        deathsElem.setAttribute( 'class', 'deaths' );
        deathsElem.setAttribute( 'title', countryName + ': ' + response[ country ][ 'deaths' ] + ' καταγεγραμμένοι θάνατοι εξαιτίας του κορωνοϊού' );
        countryElem.appendChild( deathsElem );

        var deathsElemText = document.createElement("P");
        deathsElemText.textContent = response[ country ][ 'deaths' ];
        deathsElem.appendChild( deathsElemText );

        var deathsElemNewText = document.createElement("P");
        deathsElem.appendChild( deathsElemNewText );

        var newcasesElem = document.createElement("DIV");
        newcasesElem.setAttribute( 'class', 'newcases' );
        newcasesElem.setAttribute( 'title', countryName + ': ' + response[ country ][ 'newcases' ] + ' νέα κρούσματα κορωνοϊού καταγράφηκαν τις τελευταίες 24 ώρες' );
        countryElem.appendChild( newcasesElem );

        if ( response[ country ][ 'newcases' ] > 0 ) {

            newcasesElem.textContent = '+' + response[ country ][ 'newcases' ];
            casesElemNewText.textContent = '+' + response[ country ][ 'newcases' ];

        } else {

            newcasesElem.textContent = '0';
            newcasesElem.classList.add('green');
            casesElemNewText.textContent = '0';
            casesElemNewText.classList.add('green');

        }

        var newrecoveredElem = document.createElement("DIV");
        newrecoveredElem.setAttribute( 'class', 'newrecovered' );
        newrecoveredElem.setAttribute( 'title', countryName + ': ' + response[ country ][ 'newrecoveries' ] + ' νέες περιπτώσεις ασθενών που ξεπέρασαν την ασθένεια, τις τελευταίες 24 ώρες' );
        countryElem.appendChild( newrecoveredElem );

        if ( response[ country ][ 'newrecoveries' ] > 0 ) {

            newrecoveredElem.textContent = '+' + response[ country ][ 'newrecoveries' ];
            recoveredElemNewText.textContent = '+' + response[ country ][ 'newrecoveries' ];

        } else {

            newrecoveredElem.textContent = '0';
            recoveredElemNewText.textContent = '0';

        }

        var newdeathsElem = document.createElement("DIV");
        newdeathsElem.setAttribute( 'class', 'newdeaths' );
        newdeathsElem.setAttribute( 'title', countryName + ': ' + response[ country ][ 'newdeaths' ] + ' νέοι καταγεγραμμένοι θάνατοι εξαιτίας του κορωνοϊού, τις τελευταίες 24 ώρες' );
        countryElem.appendChild( newdeathsElem );

        if ( response[ country ][ 'newdeaths' ] > 0 ) {

            newdeathsElem.textContent = '+' + response[ country ][ 'newdeaths' ];
            newdeathsElem.classList.add('red');

            deathsElemNewText.textContent = '+' + response[ country ][ 'newdeaths' ];
            deathsElemNewText.classList.add('red');

        } else {

            newdeathsElem.textContent = '0';
            deathsElemNewText.textContent = '0';

        }

    }

    closedCases = recovered + deaths;
    recoveredPerc = (( recovered * 100 ) / closedCases ).toFixed(0);
    deathsPerc = 100 - recoveredPerc;
    activeCases = cases - closedCases;

    btn_cases_update( cases );
    btn_recovered_update( recovered );
    btn_deaths_update( deaths );
    btn_closedCases_update( closedCases );
    info_recoveredPercentage_update( recoveredPerc );
    info_deathsPercentage_update( deathsPerc );
    btn_countriesNum_update( countries );
    btn_activeCases_update( activeCases );

};

manipulateCountries( data );

bindButtons();
