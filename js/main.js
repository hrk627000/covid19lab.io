var googleCodes = {
  "Andorra": 'AD',
  "United Arab Emirates": 'AE',
  "Afghanistan": 'AF',
  "Antigua and Barbuda": 'AG',
  "Anguilla": 'AI',
  "Albania": 'AL',
  "Armenia": 'AM',
  "Angola": 'AO',
  "Antarctica": 'AQ',
  "Argentina": 'AR',
  "American Samoa": 'AS',
  "Austria": 'AT',
  "Australia": 'AU',
  "Aruba": 'AW',
  "Åland Islands": 'AX',
  "Azerbaijan": 'AZ',
  "Bosnia and Herzegovina": 'BA',
  "Barbados": 'BB',
  "Bangladesh": 'BD',
  "Belgium": 'BE',
  "Burkina Faso": 'BF',
  "Bulgaria": 'BG',
  "Bahrain": 'BH',
  "Burundi": 'BI',
  "Benin": 'BJ',
  "Saint Barthélemy": 'BL',
  "Bermuda": 'BM',
  "Brunei Darussalam": 'BN',
  "Bolivia (Plurinational State of)": 'BO',
  "Bonaire, Sint Eustatius and Saba": 'BQ',
  "Brazil": 'BR',
  "Bahamas": 'BS',
  "Bhutan": 'BT',
  "Bouvet Island": 'BV',
  "Botswana": 'BW',
  "Belarus": 'BY',
  "Belize": 'BZ',
  "Canada": 'CA',
  "Cocos (Keeling) Islands": 'CC',
  "Congo, Democratic Republic of the": 'CD',
  "Central African Republic": 'CF',
  "Congo": 'CD',
  "Switzerland": 'CH',
  "Côte d'Ivoire": 'CI',
  "Cook Islands": 'CK',
  "Chile": 'CL',
  "Cameroon": 'CM',
  "China": 'CN',
  "Colombia": 'CO',
  "Costa Rica": 'CR',
  "Cuba": 'CU',
  "Cabo Verde": 'CV',
  "Curaçao": 'CW',
  "Christmas Island": 'CX',
  "Cyprus": 'CY',
  "Czechia": 'CZ',
  "CZ": 'CZ',
  "Germany": 'DE',
  "Djibouti": 'DJ',
  "Denmark": 'DK',
  "Dominica": 'DM',
  "Dominican Republic": 'DO',
  "Algeria": 'DZ',
  "Ecuador": 'EC',
  "Estonia": 'EE',
  "Egypt": 'EG',
  "Western Sahara": 'EH',
  "Eritrea": 'ER',
  "Spain": 'ES',
  "Ethiopia": 'ET',
  "Finland": 'FI',
  "Fiji": 'FJ',
  "Falkland Islands (Malvinas)": 'FK',
  "Micronesia (Federated States of)": 'FM',
  "Faroe Islands": 'FO',
  "France": 'FR',
  "Gabon": 'GA',
  "United Kingdom of Great Britain and Northern Ireland": 'GB',
  "United Kingdom": 'GB',
  "Grenada": 'GD',
  "Georgia": 'GE',
  "French Guiana": 'GF',
  "Guernsey": 'GG',
  "Ghana": 'GH',
  "Gibraltar": 'GI',
  "Greenland": 'GL',
  "Gambia": 'GM',
  "Guinea": 'GN',
  "Guadeloupe": 'GP',
  "Equatorial Guinea": 'GQ',
  "Greece": 'GR',
  "South Georgia and the South Sandwich Islands": 'GS',
  "Guatemala": 'GT',
  "Guam": 'GU',
  "Guinea-Bissau": 'GW',
  "Guyana": 'GY',
  "Hong Kong": 'HK',
  "Heard Island and McDonald Islands": 'HM',
  "Honduras": 'HN',
  "Croatia": 'HR',
  "Haiti": 'HT',
  "Hungary": 'HU',
  "Indonesia": 'ID',
  "Ireland": 'IE',
  "Israel": 'IL',
  "Isle of Man": 'IM',
  "India": 'IN',
  "British Indian Ocean Territory": 'IO',
  "Iraq": 'IQ',
  "Iran (Islamic Republic of)": 'IR',
  "Iran": 'IR',
  "Iceland": 'IS',
  "Italy": 'IT',
  "Jersey": 'JE',
  "Jamaica": 'JM',
  "Jordan": 'JO',
  "Japan": 'JP',
  "Kenya": 'KE',
  "Kyrgyzstan": 'KG',
  "Cambodia": 'KH',
  "Kiribati": 'KI',
  "Comoros": 'KM',
  "Saint Kitts and Nevis": 'KN',
  "Korea (Democratic People's Republic of)": 'KP',
  "Korea, Republic of": 'KR',
  "South Korea": 'KR',
  "Kuwait": 'KW',
  "Cayman Islands": 'KY',
  "Kazakhstan": 'KZ',
  "Lao People's Democratic Republic": 'LA',
  "Lebanon": 'LB',
  "Saint Lucia": 'LC',
  "Liechtenstein": 'LI',
  "Sri Lanka": 'LK',
  "Liberia": 'LR',
  "Lesotho": 'LS',
  "Lithuania": 'LT',
  "Luxembourg": 'LU',
  "Latvia": 'LV',
  "Libya": 'LY',
  "Morocco": 'MA',
  "Monaco": 'MC',
  "Moldova, Republic of": 'MD',
  "Montenegro": 'ME',
  "Saint Martin (French part)": 'MF',
  "Madagascar": 'MG',
  "Marshall Islands": 'MH',
  "North Macedonia": 'MK',
  "Mali": 'ML',
  "Myanmar": 'MM',
  "Mongolia": 'MN',
  "Macao": 'MO',
  "Northern Mariana Islands": 'MP',
  "Martinique": 'MQ',
  "Mauritania": 'MR',
  "Montserrat": 'MS',
  "Malta": 'MT',
  "Mauritius": 'MU',
  "Maldives": 'MV',
  "Malawi": 'MW',
  "Mexico": 'MX',
  "Malaysia": 'MY',
  "Mozambique": 'MZ',
  "Namibia": 'NA',
  "New Caledonia": 'NC',
  "Niger": 'NE',
  "Norfolk Island": 'NF',
  "Nigeria": 'NG',
  "Nicaragua": 'NI',
  "Netherlands": 'NL',
  "Norway": 'NO',
  "Nepal": 'NP',
  "Nauru": 'NR',
  "Niue": 'NU',
  "New Zealand": 'NZ',
  "Oman": 'OM',
  "Panama": 'PA',
  "Peru": 'PE',
  "French Polynesia": 'PF',
  "Papua New Guinea": 'PG',
  "Philippines": 'PH',
  "Pakistan": 'PK',
  "Poland": 'PL',
  "Saint Pierre and Miquelon": 'PM',
  "Pitcairn": 'PN',
  "Puerto Rico": 'PR',
  "Palestine, State of": 'PS',
  "Portugal": 'PT',
  "Palau": 'PW',
  "Paraguay": 'PY',
  "Qatar": 'QA',
  "Réunion": 'RE',
  "Romania": 'RO',
  "Serbia": 'RS',
  "Russian Federation": 'RU',
  "Russia": 'RU',
  "Rwanda": 'RW',
  "Saudi Arabia": 'SA',
  "Solomon Islands": 'SB',
  "Seychelles": 'SC',
  "Sudan": 'SD',
  "Sweden": 'SE',
  "Singapore": 'SG',
  "Saint Helena, Ascension and Tristan da Cunha": 'SH',
  "Slovenia": 'SI',
  "Svalbard and Jan Mayen": 'SJ',
  "Slovakia": 'SK',
  "Sierra Leone": 'SL',
  "San Marino": 'SM',
  "Senegal": 'SN',
  "Somalia": 'SO',
  "Suriname": 'SR',
  "South Sudan": 'SS',
  "Sao Tome and Principe": 'ST',
  "El Salvador": 'SV',
  "Sint Maarten (Dutch part)": 'SX',
  "Syrian Arab Republic": 'SY',
  "Eswatini": 'SZ',
  "Turks and Caicos Islands": 'TC',
  "Chad": 'TD',
  "French Southern Territories": 'TF',
  "Togo": 'TG',
  "Thailand": 'TH',
  "Tajikistan": 'TJ',
  "Tokelau": 'TK',
  "Timor-Leste": 'TL',
  "Turkmenistan": 'TM',
  "Tunisia": 'TN',
  "Tonga": 'TO',
  "Turkey": 'TR',
  "Trinidad and Tobago": 'TT',
  "Tuvalu": 'TV',
  "Taiwan, Province of China": 'TW',
  "Taiwan": 'TW',
  "Tanzania, United Republic of": 'TZ',
  "Ukraine": 'UA',
  "Uganda": 'UG',
  "United States Minor Outlying Islands": 'UM',
  "United States of America": 'US',
  "United States": 'US',
  "Uruguay": 'UY',
  "Uzbekistan": 'UZ',
  "Holy See": 'VA',
  "Saint Vincent and the Grenadines": 'VC',
  "Venezuela (Bolivarian Republic of)": 'VE',
  "Virgin Islands (British)": 'VG',
  "Virgin Islands (U.S.)": 'VI',
  "Viet Nam": 'VN',
  "Vietnam": 'VN',
  "Vanuatu": 'VU',
  "Wallis and Futuna": 'WF',
  "Samoa": 'WS',
  "Yemen": 'YE',
  "Mayotte": 'YT',
  "South Africa": 'ZA',
  "Zambia": 'ZM',
  "Zimbabwe": 'ZW'
};

var googleCodes2 = {
  "Iran (Islamic Republic of)": 'Iran',
  "Korea, Republic of": 'South Korea',
  "United States of America": 'United States',
  "United Kingdom of Great Britain and Northern Ireland": 'United Kingdom',
  "Czechia": 'CZ',
  "Russian Federation": 'Russia',
  "Taiwan, Province of China": 'Taiwan',
  "Viet Nam": 'Vietnam',
  "Palestine, State of": 'PS',
  "Brunei Darussalam": 'Brunei',
  "North Macedonia": 'MK',
  "Bolivia (Plurinational State of)": 'Bolivia',
  "Moldova, Republic of": 'Moldova',
  "Réunion": 'RE',
  "Congo, Democratic Republic of the": 'CD',
  "Congo": 'CD',
  "Venezuela (Bolivarian Republic of)": 'Venezuela',
  "Côte d'Ivoire": 'CI',
  "Curaçao": 'CW',
  "Holy See": 'VA',
  "Saint Barthélemy": 'BL',
  "Eswatini": 'SZ',
  "Virgin Islands (U.S.)": 'U.S. Virgin Islands'
};

var googleCodes3 = {
    "Iran": 'Iran (Islamic Republic of)',
    "South Korea": 'Korea, Republic of',
    "United States": 'United States of America',
    "United Kingdom": 'United Kingdom of Great Britain and Northern Ireland',
    "CZ": 'Czechia',
    "Russia": 'Russian Federation',
    "Taiwan": 'Taiwan, Province of China',
    "Vietnam": 'Viet Nam',
    "PS": 'Palestine, State of',
    "Brunei": 'Brunei Darussalam',
    "MK": 'North Macedonia',
    "Bolivia": 'Bolivia (Plurinational State of)',
    "Moldova": 'Moldova, Republic of',
    "RE": 'Réunion',
    "CD": 'Congo, Democratic Republic of the',
    "Venezuela": 'Venezuela (Bolivarian Republic of)',
    "CI": 'Côte d\'Ivoire',
    "CW": 'Curaçao',
    "VA": 'Holy See',
    "BL": 'Saint Barthélemy',
    "SZ": 'Eswatini',
    "U.S. Virgin Islands": 'Virgin Islands (U.S.)'
};

var colorAxis = {
    'markers': ['#F00', '#F00'],
    'regions': ['#ffc3c3', '#FF3434']
};



var chart;

var dataRaw = new Array();

function getGoogleAbrv( country ) {

    if ( googleCodes.hasOwnProperty( country ) === false ) {

        return country;

    }

    return googleCodes[ country ];

}

function getGoogleCode( country ) {

    if ( googleCodes2.hasOwnProperty( country ) === false ) {

        return country;

    }

    return googleCodes2[ country ];

}

function getReverseGoogleCode( country ) {

    if ( googleCodes3.hasOwnProperty( country ) === false ) {

        return country;

    }

    return googleCodes3[ country ];

}

function deactivateMapButtons() {

    var buttons = document.querySelectorAll( '#mapPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

}

function deactivateMapViews() {

    var buttons = document.querySelectorAll( '#mapPanel .mapView' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

}

function mapButtonsClick( event ) {

    toggleViewButton();

    chart.clearChart();

    unPopulateCountryCounters();

    deactivateMapButtons();

    event.target.classList.add('active');

    document.getElementById( 'map' ).setAttribute( 'data-region', event.target.getAttribute('data-region') );

    createMapData( data );

};

function mapViewsClick( event ) {

    chart.clearChart();

    deactivateMapViews();

    event.target.classList.add('active');

    document.getElementById( 'map' ).setAttribute( 'data-view', event.target.getAttribute('data-view') );

    createMapData( data );

}

function bindButtons() {

    var buttons = document.querySelectorAll( '#mapPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].addEventListener( 'click', mapButtonsClick );

    }

    var views = document.querySelectorAll( '#mapPanel .mapView' );
    var viewsNum = views.length;

    for ( var i = 0 ; i < viewsNum ; i++ ) {

        views[ i ].addEventListener( 'click', mapViewsClick );

    }

};

function toggleViewButton( country = null, status = null ) {

    var views = document.querySelectorAll( '#mapPanel .mapView' );
    var viewsNum = views.length;

    var mapCountryName = document.querySelector( '#mapPanel .mapCountryName' ); //console.log( mapCountryName );

    mapCountryName.style.color = 'rgb(221, 221, 221)';

    mapCountryName.setAttribute( 'data-countrycode', 'world' );

    if ( country !== null ) {

        for ( var i = 0 ; i < viewsNum ; i++ ) {

            views[ i ].style.opacity = 0;

        }

        mapCountryName.textContent = translateCountry( country );

        mapCountryName.setAttribute( 'data-countrycode', googleCodes[ country ] );

        if ( status === 'error' ) {

            mapCountryName.style.color = 'rgb(255, 63, 63)';

            mapCountryName.setAttribute( 'data-countrycode', 'world' );

        }

        mapCountryName.style.display = 'inline-block';

    } else {

        for ( var i = 0 ; i < viewsNum ; i++ ) {

            views[ i ].style.opacity = 1;

        }

        mapCountryName.textContent = '';

        mapCountryName.style.display = 'none';

    }

}

function createCitiesData( countryGiven = null ) {

    country = getReverseGoogleCode( countryGiven ); //console.log( country );

    if ( country !== null ) {

        toggleViewButton( country );

    }

    var dataCities = new Array();

    dataCities[0] = [ 'Lat', 'Lon', 'Name', 'Cases' ];

    if ( cities.hasOwnProperty( country ) === false ) {

        toggleViewButton( 'Not enough data at the moment', 'error' );

        return;

    }

    for ( var i=0 ; i<cities[ country ].length ; i++ ) {

        var cityName = cities[ country ][ i ][ 'Name' ];

        if ( cityName === null ) { cityName = country };

        var cityData = new Array();
        cityData.push( cities[ country ][ i ][ 'Lat' ] );
        cityData.push( cities[ country ][ i ][ 'Lon' ] );
        cityData.push( cityName );
        cityData.push( cities[ country ][ i ][ 'confirmed' ] );

        dataCities.push( cityData );

    }

    chart.clearChart();

    deactivateMapButtons();

    populateCountryCounters( country );

    createMap( dataCities, country, 'markers' );

}

function createMap ( data, region = null, displayMode = 'regions' ) {

    google.charts.load('current', {
        'packages':['geochart']
    });

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( data );

      var options = {
          colorAxis: {colors: colorAxis[ displayMode ] },
          backgroundColor: '#181818',
          datalessRegionColor: '#ddd',
          magnifyingGlass: {enable: true, zoomFactor: 5.0},
          region: getGoogleAbrv( region ),
          displayMode: displayMode
      };

      chart = new google.visualization.GeoChart(document.getElementById('map'));

      chart.draw(dataGoogle, options);

      if ( displayMode === 'regions' ) {

          google.visualization.events.addListener(chart, 'select', function( e ){

              var selectInfo = chart.getSelection();

              var row = selectInfo[0].row + 1;

              createCitiesData( data[ row ][ 0 ] );

          });

      }

    });

};

function createMapData( response ) {

    var region  = document.getElementById( 'map' ).getAttribute( 'data-region' );
    var view    = document.getElementById( 'map' ).getAttribute( 'data-view' );

    dataRaw = new Array();

    if ( view === 'country' ) {

        dataRaw[0] = [ 'Country', 'Cases', 'Deaths' ];

        for ( var country in response ) {

            if ( country === "Cruise Ship (Princess Diamond)" ) { continue; }
            if ( country === "Channel Islands" ) { continue; }
            if ( country === "Saint Martin (French part)" ) { continue; }

            var countryData = new Array();
            countryData.push( getGoogleCode( country ) );
            countryData.push( response[ country ][ 'confirmed' ] );
            countryData.push( response[ country ][ 'deaths' ] );

            dataRaw.push( countryData );

        }

        createMap( dataRaw, region );

    } else if ( view === '24' ) {

        dataRaw[0] = [ 'Country', 'New Cases', 'New Deaths' ];

        for ( var country in response ) {

            if ( country === "Cruise Ship (Princess Diamond)" ) { continue; }
            if ( country === "Channel Islands" ) { continue; }
            if ( country === "Saint Martin (French part)" ) { continue; }

            var newCases = response[ country ][ 'newcases' ];

            if ( newCases < 0 ) { newCases = 0; }

            var countryData = new Array();
            countryData.push( getGoogleCode( country ) );
            countryData.push( newCases );
            countryData.push( response[ country ][ 'newdeaths' ] );

            dataRaw.push( countryData );

        }

        createMap( dataRaw, region );

    } else if ( view === 'fatality' ) {

        dataRaw[0] = [ 'Country', 'Fatality Rate', {role: 'tooltip', p: {html: 'true'}} ];

        for ( var country in response ) {

            if ( country === "Cruise Ship (Princess Diamond)" ) { continue; }
            if ( country === "Channel Islands" ) { continue; }
            if ( country === "Saint Martin (French part)" ) { continue; }

            var fatality = parseFloat( ( ( response[ country ][ 'deaths' ] / response[ country ][ 'confirmed' ] ) * 100 ).toFixed(2) );

            if ( fatality < 0 ) { fatality = 0; }

            var countryData = new Array();
            countryData.push( getGoogleCode( country ) );
            countryData.push( fatality );
            countryData.push( 'Fatality Rate: ' + fatality + '%' );

            dataRaw.push( countryData );

        }

        createMap( dataRaw, region );

    } else if ( view === 'cases' ) {

        var dataCities = new Array();

        dataCities[0] = [ 'Lat', 'Lon', 'Name', 'Cases' ];

        for ( var country in cities ) {

            if ( country === "Cruise Ship (Princess Diamond)" ) { continue; }
            if ( country === "Channel Islands" ) { continue; }
            if ( country === "Saint Martin (French part)" ) { continue; }

            for ( var i=0 ; i<cities[ country ].length ; i++ ) {

                var cityName = cities[ country ][ i ][ 'Name' ];

                if ( cityName === null ) { cityName = country };

                var cityData = new Array();
                cityData.push( cities[ country ][ i ][ 'Lat' ] );
                cityData.push( cities[ country ][ i ][ 'Lon' ] );
                cityData.push( cityName );
                cityData.push( cities[ country ][ i ][ 'confirmed' ] );

                dataCities.push( cityData );

            }

        }

        createMap( dataCities, region, 'markers' );

    }

};

function arrangeFooter() {

    var vw = Math.max( document.documentElement.clientWidth, window.innerWidth || 0) ;

    if ( vw > 0 && vw < 1023 ) {

        var footer = document.getElementById( 'footer' );

        var countriesPanel = document.getElementById( 'countries' );

        countriesPanel.parentNode.appendChild( footer );

    }

}

createMapData( data, 'total' );

bindButtons();

arrangeFooter();
